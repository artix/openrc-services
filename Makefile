VERSION=0.1

SYSCONFDIR = /etc
CONFDDIR = $(SYSCONFDIR)/conf.d
INITDDIR = $(SYSCONFDIR)/init.d
RLVLDIR = $(SYSCONFDIR)/runlevels

INITD_SYSTEM = \
	$(wildcard system/init.d/*)

CONFD_SYSTEM = \
	$(wildcard system/conf.d/*)

INITD_WORLD = \
	$(wildcard world/init.d/*)

CONFD_WORLD = \
	$(wildcard world/conf.d/*)

AUDIT_RULES = \
	$(wildcard misc/audit/*)

WPA_SCRIPT = \
	$(wildcard scripts/wpa/*.sh)

DIRMODE = -dm0755
FILEMODE = -m0644
MODE =  -m0755
LN = ln -snf

install_common:
	install $(DIRMODE) $(DESTDIR){$(CONFDDIR),$(INITDDIR)}
	install $(DIRMODE) $(DESTDIR)$(RLVLDIR)/{sysinit,boot,default}

install_system: install_common
	install $(FILEMODE) $(CONFD_SYSTEM) $(DESTDIR)$(CONFDDIR)
	install $(MODE) $(INITD_SYSTEM) $(DESTDIR)$(INITDDIR)

	$(LN) $(INITDDIR)/udev $(DESTDIR)$(RLVLDIR)/sysinit/udev
	$(LN) $(INITDDIR)/udev-trigger $(DESTDIR)$(RLVLDIR)/sysinit/udev-trigger

	$(LN) $(INITDDIR)/kmod-static-nodes $(DESTDIR)$(RLVLDIR)/sysinit/kmod-static-nodes

	$(LN) $(INITDDIR)/esysusers $(DESTDIR)$(RLVLDIR)/boot/esysusers

	$(LN) $(INITDDIR)/elogind $(DESTDIR)$(RLVLDIR)/boot/elogind

	$(LN) $(INITDDIR)/etmpfiles-dev $(DESTDIR)$(RLVLDIR)/sysinit/etmpfiles-dev
	$(LN) $(INITDDIR)/etmpfiles-setup $(DESTDIR)$(RLVLDIR)/boot/etmpfiles-setup

	$(LN) $(INITDDIR)/dbus $(DESTDIR)$(RLVLDIR)/default/dbus

	for num in 1 2 3 4 5 6; do \
		$(LN) $(INITDDIR)/agetty $(DESTDIR)$(INITDDIR)/agetty.tty$$num; \
		$(LN) $(INITDDIR)/agetty.tty$$num $(DESTDIR)$(RLVLDIR)/default/agetty.tty$$num; \
	done

	install $(DIRMODE) $(DESTDIR)$(SYSCONFDIR)/wpa_supplicant
	install $(MODE) $(WPA_SCRIPT) $(DESTDIR)$(SYSCONFDIR)/wpa_supplicant

	install $(DIRMODE) $(DESTDIR)$(SYSCONFDIR)/audit
	install $(FILEMODE) $(AUDIT_RULES) $(DESTDIR)$(SYSCONFDIR)/audit

install_world: install_common
	install $(FILEMODE) $(CONFD_WORLD) $(DESTDIR)$(CONFDDIR)
	install $(MODE) $(INITD_WORLD) $(DESTDIR)$(INITDDIR)

install: install_system install_world

PHONY: install_system install_world
